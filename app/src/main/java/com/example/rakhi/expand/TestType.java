package com.example.rakhi.expand;

import java.util.ArrayList;

/**
 * Created by Rakhi on 07-09-2017.
 */
public class TestType {
    private String name;
    private ArrayList<TestDetailsIn> testDetailsList = new ArrayList<TestDetailsIn>();


    public TestType(String name, ArrayList<TestDetailsIn> testDetailsList) {
        this.name = name;
        this.testDetailsList = testDetailsList;
    }

    public String getName() {
        return name;
    }

    public ArrayList<TestDetailsIn> getTestDetailsList() {
        return testDetailsList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTestDetailsList(ArrayList<TestDetailsIn> testDetailsList) {
        this.testDetailsList = testDetailsList;
    }
}

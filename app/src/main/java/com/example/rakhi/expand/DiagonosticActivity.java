package com.example.rakhi.expand;

import android.app.SearchManager;
import android.content.Context;
import android.database.DataSetObserver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;

public class DiagonosticActivity extends AppCompatActivity implements
        SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    ArrayList<String> value=new ArrayList<>();
    HashMap<String,ArrayList<String>>hsm=new HashMap<>();


    ArrayList<String> children;
    DatabaseReference databaseReference;


    SearchView search;
    private TextWatcher filterTextWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagonostic);
        databaseReference= FirebaseDatabase.getInstance().getReference().child("diagonostic details");

        expandableListView=(ExpandableListView)findViewById(R.id.exp_list);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) findViewById(R.id.search);

        expandableListAdapter=new ExpandableListAdapter(DiagonosticActivity.this,value,hsm);
        expandableListView.setAdapter(expandableListAdapter);

        databaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                DiagonosticDetails diag=dataSnapshot.getValue(DiagonosticDetails.class);
                Log.i("Diag Deat",String.valueOf(diag));
                String Money=diag.getMoney();
                Log.i("test Money",Money);
                children=new ArrayList<String>();
                children.add(diag.getTestDetails());
                children.add(diag.getLocation());
                children.add(diag.getName());
                hsm.put(diag.getTestType(),children);
                value.add(diag.getTestType());

                Log.i("Hashmap value",String.valueOf(hsm));

                Log.i("arraylist",String.valueOf(value));

                expandableListAdapter.notifyDataSetChanged();//getview method of messageadapter is called and the observer of the listview refreshes all the data and redraws the listrow
                expandableListAdapter.registerDataSetObserver(new DataSetObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        Toast.makeText(DiagonosticActivity.this, "Dataset changed", Toast.LENGTH_SHORT).show();
/*
                        chat_list.setSelection(adapter.getCount()-1);
*/
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

       /* search.addTextChangedListener(filterTextWatcher);

        filterTextWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s) {
                ((Filterable) (expandableListAdapter)).getFilter().filter(search.getText().toString());
            }
        };*/


        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        value.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(getApplicationContext(),
                        value.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getApplicationContext(),
                        value.get(groupPosition)
                                + " : "
                                + hsm.get(
                                value.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }


    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        expandableListAdapter.filterData(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.i("on","hughjg");
        expandableListAdapter.filterData(newText);
        return false;
    }
}


package com.example.rakhi.expand;

/**
 * Created by Rakhi on 02-09-2017.
 */
public class DiagonosticDetails {
    String Money="";
    String TestDetails="";
    String Name="";
    String Timing="";
    String TestType="";
    String Location="";


    public void setMoney(String money) {
        Money = money;
    }

    public void setTestDetails(String testDetails) {
        TestDetails = testDetails;
    }


    public void setName(String name) {
        Name = name;
    }

    public void setTiming(String timing) {
        Timing = timing;
    }


    public void setTestType(String testType) {
        TestType = testType;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getMoney() {

        return Money;
    }

    public String getTestDetails() {
        return TestDetails;
    }

    public String getName() {
        return Name;
    }

    public String getTiming() {
        return Timing;
    }

    public String getTestType() {
        return TestType;
    }

    public String getLocation() {
        return Location;
    }
}

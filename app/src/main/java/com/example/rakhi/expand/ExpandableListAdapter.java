package com.example.rakhi.expand;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Rakhi on 02-09-2017.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    Context con;
    ArrayList<String> value;
    ArrayList<String> Originalvalue;

    HashMap<String, ArrayList<String>> hsm;

    public ExpandableListAdapter(Context context, ArrayList<String> value, HashMap<String, ArrayList<String>> hsm) {
        this.con = context;
        this.value = value;
        this.hsm = hsm;
        this.Originalvalue = value;
    }


    @Override
    public int getGroupCount() {
        Log.i("group count", "jhcsfjf");
        return value.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        Log.d("get children count", "bshadbjakscb");
        return this.hsm.get(this.value.get(groupPosition))
                .size();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Log.i("get child value", "");
        return this.hsm.get(this.value.get(groupPosition))
                .get(childPosition);
    }


    @Override
    public Object getGroup(int groupPosition) {
        Log.d("getgroup", "fgyhdtrdffchghy");
        return value.get(groupPosition);
    }


    @Override
    public long getGroupId(int groupPosition) {
        Log.d("getgroupid", "bjhadbkjsjdialsfjldsv");
        return groupPosition;

    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.con
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.expandedListItem);
        expandedListTextView.setText(childText);


        return convertView;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        Log.i("group view", "sdfgreeyreh");

        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.con.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.group_header, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setTextColor(con.getResources().getColor(R.color.colorPrimaryDark));
        listTitleTextView.setText(headerTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    public void filterData(String query) {

        Log.i("enter","jhiu");
        query = query.toLowerCase();
        Log.v("header array list", String.valueOf(value.size()));
        value.clear();
        ArrayList<String> newList = new ArrayList<String>();

        if (query.isEmpty()) {
            value.addAll(Originalvalue);
        } else {
            for (int i = 0; i < Originalvalue.size(); i++) {
                if (Originalvalue.get(i).toLowerCase().contains(query)) {
                    newList.add(Originalvalue.get(i));
                    Log.i("new list",String.valueOf(newList));

                }
            }

            if(newList.size() > 0){
               value.addAll(newList);

            }

            notifyDataSetChanged();


        }
    }
}

package com.example.rakhi.expand;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import junit.framework.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Rakhi on 02-09-2017.
 */
public class ExpandableListAdapter_one extends BaseExpandableListAdapter{

    Context con;
    private Context context;
    private ArrayList<TestType> testTypeList;
    private ArrayList<TestType> originalTestTypeList;

    public ExpandableListAdapter_one(Context context,ArrayList<TestType> testTypeList){
        this.con=context;
        this.testTypeList=new ArrayList<TestType>();
        this.originalTestTypeList=new ArrayList<TestType>();
        originalTestTypeList.addAll(testTypeList);
        this.testTypeList.addAll(testTypeList);
    }


    @Override
    public int getGroupCount() {
        Log.i("group count","jhcsfjf");
        return testTypeList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        Log.d("get children count","bshadbjakscb");
        ArrayList<TestDetailsIn> testDetailsList = testTypeList.get(groupPosition).getTestDetailsList();
        return testDetailsList.size();
    }
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        Log.i("get child value", "");
        ArrayList<TestDetailsIn> testDetailsList = testTypeList.get(groupPosition).getTestDetailsList();
        return testDetailsList.get(childPosition);

    }


    @Override
    public Object getGroup(int groupPosition) {
        Log.d("getgroup","fgyhdtrdffchghy");
        return testTypeList.get(groupPosition);
    }


    @Override
    public long getGroupId(int groupPosition) {
        Log.d("getgroupid","bjhadbkjsjdialsfjldsv");
        return groupPosition;

    }



    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        TestDetailsIn testDetails= (TestDetailsIn) getChild(groupPosition,childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.child_row, null);
        }

        TextView code = (TextView) convertView.findViewById(R.id.code);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView population = (TextView) convertView.findViewById(R.id.population);
        code.setText(testDetails.getName().trim());
        name.setText(testDetails.getTestDetails().trim());
        population.setText(testDetails.getLocation());

        return convertView;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        Log.i("group view","sdfgreeyreh");

        TestType testType = (TestType) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.group_row, null);
        }

        TextView heading = (TextView) convertView.findViewById(R.id.heading);
        heading.setText(testType.getName().trim());
        return convertView;
    }
    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


   /* public void filterData(String query){

        query = query.toLowerCase();
        Log.v("MyListAdapter", String.valueOf(testTypeList.size()));
        testTypeList.clear();

        if(query.isEmpty()){
            testTypeList.addAll(originalTestTypeList);
        }
        else {

            for(TestType testType: originalTestTypeList){

                ArrayList<TestDetailsIn> countryList = testType.getTestDetailsList();
                ArrayList<TestDetailsIn> newList = new ArrayList<TestDetailsIn>();

                for(TestDetailsIn testDetailsIn: countryList){
                    if(testDetailsIn.getTestDetails().toLowerCase().contains(query) ||
                            testDetailsIn.getName().toLowerCase().contains(query)){
                        newList.add(testDetailsIn);
                    }
                }
                if(newList.size() > 0){
                    TestType nContinent = new TestType(testType.getName(),newList);
                    testTypeList.add(nContinent);
                }
            }
        }

        Log.v("MyListAdapter", String.valueOf(testTypeList.size()));
        notifyDataSetChanged();

    }*/

}

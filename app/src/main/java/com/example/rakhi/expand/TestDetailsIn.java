package com.example.rakhi.expand;

/**
 * Created by Rakhi on 07-09-2017.
 */
public class TestDetailsIn {
    private String TestDetails = "";
    private String Location = "";
    private String Name = "";

    public TestDetailsIn(String testDetails, String location, String name) {
        super();
        TestDetails = testDetails;
        Location = location;
        Name = name;
    }


    public String getTestDetails() {
        return TestDetails;
    }

    public String getLocation() {
        return Location;
    }

    public String getName() {
        return Name;
    }

    public void setTestDetails(String testDetails) {
        TestDetails = testDetails;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public void setName(String name) {
        Name = name;
    }
}
